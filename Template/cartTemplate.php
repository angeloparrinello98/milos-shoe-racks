<h3><i class="fas fa-shopping-cart" style="color: black; font-size: 70px;"> CARRELLO</i></h3>

<!-- Cart -->
<div class="container mb-2">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Prodotti</th>
                            <th scope="col">Taglia</th>
                            <th scope="col" class="text-center">Quantità</th>
                            <th scope="col" class="text-right">Prezzo</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($templateParams["carrellata"] as $articolo): ?>
                        <tr>
                            <td><img src="<?php echo UPLOAD_DIR.$articolo["Codice_Immagine"]?>" style="width: 50px; height:50px"/> </td>
                            <td class="text"><a href="article.php?id=<?php echo $articolo["Codice_Articolo"]?>" style="text-decoration: none; color: black"><?php echo $articolo["Codice_Articolo"]?></a></td>
                            <td class="text-left"><?php echo $articolo["Taglia"] ?></td>
                            <td class="text-center"><?php echo $articolo["Quantità"] ?></td>
                            <td class="text-right"><?php echo $articolo["Prezzo"] ?></td>
                            <td class="text-right"><a style="text-decoration: none; color: black" href="delete.php?id=<?php echo $articolo["Codice_Articolo"]?>,<?php echo $articolo["Taglia"]?>" role="button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-circle-fill" viewBox="0 0 16 16">
                                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z"/>
                                </svg> </a> </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <p style="text-align: center;">Sub-Total: <?php echo $templateParams["subtotale"]?>€</p>
                </div>
                <div class="col-4">
                    <p style="text-align: center;">Shipping: 6.9€</p>
                </div>
                <div class="col-4">
                    <p style="text-align: center;">Total: <?php echo $templateParams["totale"]?>€</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12  col-md-6">
                    <a class="btn btn-primary btn-lg btn-block" href="index.php">Continua lo Shopping</a>
                </div>
                <div class="col-12 col-md-6 text-right">
                <div class="dropdown">
                    <button class="btn btn-primary btn-lg btn-block dropdown-toggle" style="text-decoration: none;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Checkout
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php foreach($templateParams["indirizzi"] as $indirizzo): ?>
                        <a class="dropdown-item" href="checkout.php?id=<?php echo $indirizzo["Progressivo"] ?>"><?php echo $indirizzo["Nome_Indirizzo"]?></a>
                    <?php endforeach; ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Cart -->
</div>
</div>