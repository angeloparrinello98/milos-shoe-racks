<section class="container my-5">
        <div class="text-center">
            <h3>Aggiungi un nuovo indirizzo</h3>
            <p>Tutti i campi sono obbligatori</p>
        </div>
        <form method="POST" >
        
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" value="<?php if(isset($_GET["id"])){ echo $templateParams["infobase"]["Nome_Indirizzo"];}?>" id="nome" name="Nome"/>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupVia">Via</label>
                    <input type="text" class="form-control" value="<?php if(isset($_GET["id"])){ echo $templateParams["infobase"]["Via"];}?>" id="formGroupViaENumeroCivico" name="Via"/>
            </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupNumeroCivico">Numero Civico</label>
                    <input type="text" class="form-control" value="<?php if(isset($_GET["id"])){ echo $templateParams["infobase"]["Civico"];}?>" id="formGroupViaENumeroCivico" name="NumeroCivico"/>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupCittà">Città</label>
                    <input type="text" class="form-control" value="<?php if(isset($_GET["id"])){ echo $templateParams["infobase"]["Citta"];}?>" id="formGroupCittà" name="Citta"/>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupProvincia">Provincia</label>
                    <input type="text" class="form-control" value="<?php if(isset($_GET["id"])){ echo $templateParams["infobase"]["Provincia"];}?>" id="formGroupProvincia" name="Provincia"/>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupCap">Cap</label>
                    <input type="text" class="form-control" value="<?php if(isset($_GET["id"])){ echo $templateParams["infobase"]["CAP"];}?>" id="formGroupCap" name="CAP"/>
                </div>
            </div>

            <div class="row justify-content-center mt-4">
                <button type="submit" name="submit" class="btn btn-primary">Aggiungi indirizzo</button>
            </div>
            
          </form>
    </section>