<!--Section: Block Content-->
        <section class="mb-5 mt-5 container">
        <form method="POST">
            <div class="row">

                <div class="col-md-4 pt-2">
                    <figure>
                        <img src="<?php echo UPLOAD_DIR.$templateParams["articoloImmagini"]["Codice_Immagine"]?>" class="img-fluid">
                    </figure>
                </div>

                <div class="col-md-6 pl-sm-2 pt-2">

                    <h5><?php echo $templateParams["articoloSingolo"]["Nome_Articolo"]?></h5>
                    <p><span class="mr-1"><strong><?php echo "€".$templateParams["articoloSingolo"]["Prezzo"]?></strong></span></p>
                    <p class="pt-1"><?php echo $templateParams["articoloSingolo"]["Descrizione"]?></p>
                    <div class="table-responsive mb-2">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td class="pl-0 pb-0 ">Quantità:</td>
                                    <td class="pb-0">Seleziona taglia:</td>
                                    <td class="pb-0">Disponibilità: <?php echo $templateParams["articoloSingolo"]["Scorta"]?></td>
                                </tr>
                                <tr>
                                    <td class="pl-0">
                                        <div class="mb-0">
                                            <input min="0" name="quantity" value="1" type="number" id="qta">
                                        </div>
                                    </td>
                                    <td>
                                        <!-- size Radio button-->
                                        <div class="mt-1">

                                            <div class="form-check form-check-inline pl-0">
                                                <input type="radio" class="form-check-input" id="small"
                                                    name="taglia" value="S" checked>
                                                <label class="form-check-label small text-uppercase"
                                                    for="small">Small</label>
                                            </div>

                                            <div class="form-check form-check-inline pl-0">
                                                <input type="radio" class="form-check-input" id="medium"
                                                    name="taglia" value="M">
                                                <label class="form-check-label small text-uppercase"
                                                    for="medium">Medium</label>
                                            </div>

                                            <div class="form-check form-check-inline pl-0">
                                                <input type="radio" class="form-check-input" id="large"
                                                    name="taglia" value="L">
                                                <label class="form-check-label small text-uppercase"
                                                    for="large">Large</label>
                                            </div>

                                        </div>
                                        <!-- size Radio button-->

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn btn-light mr-1 mb-2"
                    <?php if($templateParams["articoloSingolo"]["Scorta"] <= 0) {echo "disabled";} ?> >Aggiungi al carrello</button>
                </div>
            </div>
        </form>
        </section>
        <!--Section: Block Content-->

        <!--Classic tabs-->
        <div class="classic-tabs px-4 pt-1 pb-5">
            <ul class="nav tabs-primary nav-justified pb-3">
                <li class="nav-item menu pb-2">
                    <a class="nav-link active show position-relative text-dark" data-toggle="tab" href="#description"
                        role="tab" aria-controls="description" aria-selected="true">Descrizione
                        <div class="effect">
                        </div>
                    </a>
                </li>
                <li class="nav-item menu">
                    <a class="nav-link position-relative text-dark" data-toggle="tab" href="#info" role="tab"
                        aria-controls="info" aria-selected="false">Informazioni<div class="effect"></div></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="description" role="tabpanel"
                    aria-labelledby="description-tab">
                    <h5><?php echo $templateParams["articoloSingolo"]["Nome_Articolo"]?></h5>
                    <h6><?php echo "€".$templateParams["articoloSingolo"]["Prezzo"]?></h6>
                    <p class="pt-1"><?php echo $templateParams["articoloSingolo"]["Descrizione"]?></p>
                </div>
                <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
                    <h5>Informazioni aggiuntive</h5>
                    <table class="table table-striped table-bordered mt-3">
                        <thead>
                            <tr>
                                <th scope="row" class="w-150 dark-grey-text h6">Peso</th>
                                <th scope="row" class="w-150 dark-grey-text h6">Materiale</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><em><?php echo $templateParams["articoloSingolo"]["Peso"]."g"?></em></td>
                                <td><em><?php echo $templateParams["articoloSingolo"]["Materiale"]?></em></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <!-- Classic tabs -->