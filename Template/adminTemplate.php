    <!--Admin Section-->
    <div class="accordion" id="accordion">
    <?php foreach($templateParams["categorietotali"] as $categorie): ?>
        <div class="card">
            <div class="card-header" id="">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-center" type="button" data-toggle="collapse" data-target="#<?php echo $categorie["Nome_Categoria"] ?>" aria-expanded="true" aria-controls="">
                        <?php echo $categorie["Nome_Categoria"]?>
                    </button>
                </h2>
            </div>

            <div id="<?php echo $categorie["Nome_Categoria"] ?>" class="collapse" aria-labelledby="<?php echo $categorie["Nome_Categoria"] ?>" data-parent="#accordion">
                <div class="card-body">
                <table class="table">
                <thead>
                    <tr>
                        <th scope="col"> </th>
                        <th scope="col">Nome</th>
                        <th scope="col">Immagine</th>
                        <th scope="col">Prezzo</th>
                        <th scope="col">Data Inserimento</th>
                        <th scope="col">Scorta</th>
                        <th scope="col">Peso</th>
                        <th scope="col">Sconto</th>
                        <th scope="col">Descrizione</th>
                        <th scope="col">Materiale</th>
                        <th scope="col">Punteggio</th>
                        <th scope="col" class="text-center">Gestisci</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($templateParams[$categorie["Nome_Categoria"]] as $articoli):?>
                    <tr>
                        <td></td>
                        <td><a href="article.php?id=<?php echo $articoli["Nome_Articolo"]?>" style="text-decoration: none; color: black"><?php echo $articoli["Nome_Articolo"] ?></a></td>
                        <td><a href="article.php?id=<?php echo $articoli["Nome_Articolo"]?>" style="text-decoration: none; color: black"><img src="<?php echo UPLOAD_DIR.$articoli["Codice_Immagine"]?>" style="height:50px; width:50px; "alt=""></a></td>
                        <td><?php echo $articoli["Prezzo"] ?>€</td>
                        <td><?php echo $articoli["Data_Inserimento"] ?></td>
                        <td><?php echo $articoli["Scorta"] ?></td>
                        <td><?php echo $articoli["Peso"] ?>g</td>
                        <td><?php echo $articoli["Sconto"] ?></td>
                        <td><?php echo $articoli["Descrizione"] ?></td>
                        <td><?php echo $articoli["Materiale"] ?></td>
                        <td><?php echo $articoli["Punteggio"] ?></td>
                        <td>
                            <div class="row">
                                <div class="col-12">
                                    <a  href="update.php?id=<?php echo $articoli["Nome_Articolo"]?>" class="btn btn-primary btn-lg btn-block mb-3" style="font-size: 15px;">Modifica</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                <a class="btn btn-primary btn-lg btn-block" style="font-size: 15px;" href="removeArticle.php?id=<?php echo $articoli["Nome_Articolo"]?>" >Cancella</a>
                            </div>
                            </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    </div>
    <!--End Admin Section-->
    <div class="w-100 d-flex justify-content-center my-5"><a href="aggiungi.php" class="btn btn-primary" role="button">Aggiungi articolo</a></div>
        

    <!--notifiche-->
<section class="container mt-3">
        <h2>Controlla le tue notifiche</h2>
        <p>Controlla e gestisci tutte le tue notifiche e controlla chi ha ordinato</p>
        <div class="row">
        <?php foreach($templateParams["notificheUser"] as $notifica): ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 d-flex justify-content-center">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $notifica["Tipo"]?></h5>
                            <p class="card-text mb-0"><?php echo $notifica["Anteprima_Descrizione"]?></p>
                            <p class="card-text mb-0"><?php echo $notifica["Id_Notifica"]?></p>
                            <div class="collapse mb-2" id="veditutto<?php echo $notifica["Id_Notifica"]?>">
                                <p class="card-text mb-0"><?php echo $notifica["Descrizione"]?></p>
                            </div>
                            <a class="btn btn-primary mt-2" data-toggle="collapse" href="#veditutto<?php echo $notifica["Id_Notifica"]?>" role="button" aria-expanded="false" aria-controls="info">Vedi tutto</a>
                            <a class="btn btn-primary mt-2" role="button" href="removeNotification.php?id=<?php echo $notifica["Id_Notifica"]?>">Rimuovi</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
</section>

        <!--Password reset-->
        <section class="container mt-3">
        <h2>Vuoi cambiare la password?</h2>
        <p>Nel caso avessi bisogno di cambiare password, questa è la sezione giusta</p>
            <?php if ( isset($templateParams["pswwrong"]) ):?>
            <p><?php echo $templateParams["pswwrong"]?></p>
            <?php endif; ?>
            <?php if ( isset($templateParams["pswok"]) ):?>
            <p><?php echo $templateParams["pswok"]?></p>
            <?php endif; ?>
        <form method="POST">
            <div class="form-group row">
                <div class="col-md-6 col-9">
                    <input type="password" class="form-control mb-2" placeholder="Password" name="newpassword" required>
                    <input type="password" class="form-control" placeholder="Conferma Password" name="confermaPassword" required>
                    <button type="submit" class="btn btn-primary mt-2 ">Vai</button>
                </div>
            </div>
        </form>
        <div class="w-100 d-flex justify-content-center mt-5"><a href="logout.php" class="btn btn-primary" role="button">Esci</a></div>
        </section>
