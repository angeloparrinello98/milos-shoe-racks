<!--section categories-->
<h3 class="text-uppercase"><?php echo $idcategoria;?></h3>
        <div class="container">
            <div class="col-12 col-sm-12">
                <div class="row">
                <?php foreach($templateParams["articoliCategorie"] as $dellny): ?>
                    <div class="row-image col-12 col-md-6 col-lg-4">
                        <a href="article.php?id=<?php echo $dellny["Nome_Articolo"]?>"><img src="<?php echo UPLOAD_DIR.$dellny["Codice_Immagine"]?>" class="img-fluid" alt=""
                                style="display: block;"></a>
                        <figcaption>
                            <a class="NameItem" href="article.php?id=<?php echo $dellny["Nome_Articolo"]?>"><?php echo $dellny["Nome_Articolo"]?></a>
                            <p style="font-weight: bolder;">€<?php echo $dellny["Prezzo"]?></p>
                        </figcaption>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>


        </div>