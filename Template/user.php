<!--notifiche-->
<section class="container mt-3">
        <h2>Controlla le tue notifiche</h2>
        <p>Da questa sezione puoi controllare e rimuovere tutte le tue notifiche</p>
        <div class="row">
        <?php foreach($templateParams["notificheUser"] as $notifica): ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 d-flex justify-content-center">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $notifica["Tipo"]?></h5>
                            <p class="card-text mb-0"><?php echo $notifica["Anteprima_Descrizione"]?></p>
                            <p class="card-text mb-0"><?php echo $notifica["Id_Notifica"]?></p>
                            <div class="collapse mb-2" id="veditutto<?php echo $notifica["Id_Notifica"]?>">
                                <p class="card-text mb-0"><?php echo $notifica["Descrizione"]?></p>
                            </div>
                            <a class="btn btn-primary mt-2" data-toggle="collapse" href="#veditutto<?php echo $notifica["Id_Notifica"]?>" role="button" aria-expanded="false" aria-controls="info">Vedi tutto</a>
                            <a class="btn btn-primary mt-2" role="button" href="removeNotification.php?id=<?php echo $notifica["Id_Notifica"]?>">Rimuovi</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
</section>


<!--Orders handler-->
<section class="container mt-3">
        <h2>Controlla i tuoi ordini</h2>
        <p>Da questa sezione puoi controllare i dettagli dei tuoi ordini</p>
        <div class="row">
        <?php $i = 0; ?>
        <?php foreach($templateParams["ordiniUser"] as $ordini): ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 d-flex justify-content-center">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <?php $i++; ?>
                            <h5 class="card-title"><?php echo $ordini["Codice_Articolo"] ?></h5>
                            <p class="card-text mb-0">Acquistato il <?php echo $ordini["Data_Ordine"]?></p>
                            <p class="card-text mb-0">Arriverà il <?php echo $ordini["Data_Consegna"]?></p>
                            <p class="card-text mb-0">A <?php echo $ordini["Nome_Indirizzo"]?></p>
                            <div class="collapse mb-2" id="info<?php echo $i ?>">
                                <p class="card-text mb-0">Con codice <?php echo $ordini["Id_Ordine"]?></p>
                                <p class="card-text mb-0">Taglia <?php echo $ordini["Taglia"]?></p>
                                <p class="card-text">Quantità <?php echo $ordini["Quantità"]?></p>
                            </div>
                            <a class="btn btn-primary mt-2" data-toggle="collapse" href="#info<?php echo $i ?>" role="button" aria-expanded="false" aria-controls="info">
                                Info
                            </a>
                        </div>
                    </div>
                </div>
        <?php endforeach; ?>

        </div>
</section>

<!--Adress handler-->
<section class="container mt-3">
    <h2>Gestisci i tuoi indirizzi</h2>
    <p>Da questa sezione puoi controllare e gestire i tuoi indirizzi</p>
    <div class="row">
        
        <?php foreach($templateParams["indirizziUser"] as $indirizzi): ?>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 d-flex justify-content-center">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $indirizzi["Nome_Indirizzo"] ?></h5>
                    <p class="card-text mb-0"><?php echo $indirizzi["Via"]." ".$indirizzi["Civico"] ?></p>
                    <p class="card-text mb-0"><?php echo $indirizzi["Citta"]." ".$indirizzi["Provincia"] ?></p>
                    <p class="card-text"><?php echo $indirizzi["CAP"] ?></p>
                    <a href="address.php?id=<?php echo $indirizzi["Progressivo"]?>" class="card-link">Modifica</a>
                    
                    <a href="remove.php?id=<?php echo $indirizzi["Progressivo"]?>" class="card-link">Rimuovi</a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 d-flex justify-content-center">
            <div class="card d-flex justify-content-center align-content-center" style="width: 18rem;">
                <a href="address.php" class="btn shadow-none">
                    <div class="card-body text-center">
                        <h6 class="card-title text-muted">Aggiungi indirizzo</h6>
                        <div class="card-text">
                            <svg class="white" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                                fill="currentColor" class="bi bi-plus-square-dotted" viewBox="0 0 16 16">
                                <path
                                    d="M2.5 0c-.166 0-.33.016-.487.048l.194.98A1.51 1.51 0 0 1 2.5 1h.458V0H2.5zm2.292 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zm1.833 0h-.916v1h.916V0zm1.834 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zM13.5 0h-.458v1h.458c.1 0 .199.01.293.029l.194-.981A2.51 2.51 0 0 0 13.5 0zm2.079 1.11a2.511 2.511 0 0 0-.69-.689l-.556.831c.164.11.305.251.415.415l.83-.556zM1.11.421a2.511 2.511 0 0 0-.689.69l.831.556c.11-.164.251-.305.415-.415L1.11.422zM16 2.5c0-.166-.016-.33-.048-.487l-.98.194c.018.094.028.192.028.293v.458h1V2.5zM.048 2.013A2.51 2.51 0 0 0 0 2.5v.458h1V2.5c0-.1.01-.199.029-.293l-.981-.194zM0 3.875v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 5.708v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 7.542v.916h1v-.916H0zm15 .916h1v-.916h-1v.916zM0 9.375v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .916v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .917v.458c0 .166.016.33.048.487l.98-.194A1.51 1.51 0 0 1 1 13.5v-.458H0zm16 .458v-.458h-1v.458c0 .1-.01.199-.029.293l.981.194c.032-.158.048-.32.048-.487zM.421 14.89c.183.272.417.506.69.689l.556-.831a1.51 1.51 0 0 1-.415-.415l-.83.556zm14.469.689c.272-.183.506-.417.689-.69l-.831-.556c-.11.164-.251.305-.415.415l.556.83zm-12.877.373c.158.032.32.048.487.048h.458v-1H2.5c-.1 0-.199-.01-.293-.029l-.194.981zM13.5 16c.166 0 .33-.016.487-.048l-.194-.98A1.51 1.51 0 0 1 13.5 15h-.458v1h.458zm-9.625 0h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zm1.834-1v1h.916v-1h-.916zm1.833 1h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
                            </svg>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!--Password reset-->
<section class="container mt-3">
<h2>Vuoi cambiare la password?</h2>
<p>Nel caso tu abbia necessità di cambiare password, da questa sezione potrai farlo comodamente</p>
    <?php if ( isset($templateParams["pswwrong"]) ):?>
    <p><?php echo $templateParams["pswwrong"]?></p>
    <?php endif; ?>
    <?php if ( isset($templateParams["pswok"]) ):?>
    <p><?php echo $templateParams["pswok"]?></p>
    <?php endif; ?>
<form method="POST">
    <div class="form-group row">
        <div class="col-md-6 col-9">
            <input type="password" class="form-control mb-2" placeholder="Password" name="newpassword" required>
            <input type="password" class="form-control" placeholder="Conferma Password" name="confermaPassword" required>
            <button type="submit" class="btn btn-primary mt-2 ">Vai</button>
        </div>
    </div>
</form>
<div class="w-100 d-flex justify-content-center mt-5"><a href="logout.php" class="btn btn-primary" role="button">Esci</a></div>
</section>

    
