<section class="container my-5">
        <div class="text-center">
            <h3>Aggiungi un articolo</h3>
        </div>
        <?php if(isset($templateParams["msg"])):?>
            <p><?php echo $templateParams["msg"]?></p>
        <?php endif;?>
        <form class="mt-5" method="POST" enctype="multipart/form-data">
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="nome">Nome:</label>
                    <input name="nome" type="text" class="form-control" id="nome">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupPrezzo">Immagine:</label>
                    <input name="imgarticolo" type="file"  class="form-control" id="imgarticolo">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="prezzo">Categoria:</label>
                    <input name="categoria" type="text" class="form-control" id="prezzo">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="prezzo">Prezzo:</label>
                    <input name="prezzo" type="text" class="form-control" id="prezzo">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="peso">Peso:</label>
                    <input name="peso" type="text" class="form-control" id="peso">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="scorta">Scorta:</label>
                    <input name="scorta" type="text" class="form-control" id="scorta">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="sconto">Sconto:</label>
                    <input name="sconto" type="text" class="form-control" id="sconto">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="descrizione">Descrizione:</label>
                    <input name="descrizione" type="text" class="form-control" id="descrizione">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="materiale">Materiale:</label>
                    <input name="materiale" type="text" class="form-control" id="materiale">
                </div>
            </div>
            <div class="row justify-content-center mt-4">
                <button type="submit" class="btn btn-primary">Aggiungi</button>
            </div>
        </form>
    </section>