     <!--Registration Form-->
     <div id="login-form-container" class="row justify-content-center">
        <?php if(isset($templateParams["err"])): ?>
            <p><?php echo $templateParams["err"]?></p>
        <?php endif; ?>
        <div class="col-12 col-md-8 col-lg-6">
            <form class="p-4" method="POST">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="password1">Password</label>
                  <input type="password" class="form-control" name="password1" id="password1">
                </div>
                <div class="form-group">
                    <label for="password2">Conferma password</label>
                    <input type="password" class="form-control" name="password2" id="password2">
                </div>
                    <button type="submit" class="btn btn-primary">Registrati</button>
              </form>
        </div>
    </div>
    <!--Registration Form-->