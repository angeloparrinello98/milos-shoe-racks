<div id="login-form-container" class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-6">
                    <form class="p-4" action="#" method="POST">
                        <?php if(isset($templateParams["erroreLogin"])):?>
                        <p><?php echo $templateParams["erroreLogin"]?></p>
                        <?php endif; ?>
                        <div class="form-group">
                          <label for="email">Email</label>
                          <input type="text" class="form-control" id="email" aria-describedby="emailHelp" name="email">
                          <small id="emailHelp" class="form-text text-muted">La tua email non verrà condivisa con nessun altro</small>
                        </div>
                        <div class="form-group">
                          <label for="password">Password</label>
                          <input type="password" class="form-control" id="password" name="password">
                        </div>
                            <button type="submit" name="submit" class="btn btn-primary">Accedi</button>
                            <a href="registration.php" class="btn btn-primary">Registrati</a>
                    </form>
                </div>
</div>