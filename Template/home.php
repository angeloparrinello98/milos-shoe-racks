        <!--Carosello-->
        <section class="bottom-margined">
        <!-- bootstrap carousel-->
        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="Upload/Carousel_Images/CaroselloIMG1.png" class="img-fluid dual-image-mb" alt="">
                    <img src="Upload/collana_con_onda.jpg" class="img-fluid dual-image" alt="">
                    <div class="carousel-caption">
                        
                        <h2>Collane</h2>
                        <p>Vieni a scoprire le nostre Collane</p>
                        
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="Upload/Carousel_Images/CaroselloIMG2.png" class="img-fluid dual-image-mb" alt="">
                    <img src="Upload/braccialetto_cordino_rosso.jpg" class="img-fluid dual-image" alt="">
                    <div class="carousel-caption">
                        
                        <h2>Bracciali</h2>
                        <p>Vieni a scoprire i nostri splendidi Bracciali</p>
                        
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="Upload/Carousel_Images/CaroselloIMG3.png" class="img-fluid dual-image-mb" alt="">
                    <img src="Upload/anello_regolabile.jpg" class="img-fluid dual-image" alt="">
                    <div class="carousel-caption">
                        
                        <h2>Anelli</h2>
                        <p>Vieni a scoprire i nostri splendidi Anelli</p>
                      
                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </section>
    <!--Carosello-->
    <div class="container">

            <div class="row">

                <div class="row-image img-hover-zoom col-12 col-sm-12 col-md-6">
                <a href="category.php?id=Anelli">
                    <img src="<?php echo UPLOAD_DIR.$templateParams["categorieAnelli"][0]["Codice_Immagine"]?>" class="img-fluid" alt="">
                    <div class="text-hover" style="font-size: 50px;"><?php echo $templateParams["categorieAnelli"][0]["Nome_Categoria"]?></div>
                </a>
                </div> 

                <div class="row-image col-12 col-sm-12 col-md-6">

                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="row">
                               <?php for($i=0; $i<2 ; $i++): ?>
                                <div class="img-hover-zoom col-6 col-sm-6">
                                    <a href="article.php?id=<?php echo $templateParams["articoliHome"][$i]["Nome_Articolo"] ?>">
                                        <img src="<?php echo UPLOAD_DIR.$templateParams["articoliHome"][$i]["Codice_Immagine"]?>" class="img-fluid" alt="">
                                        <div class="text-hover" style="font-size: 15px"><?php echo $templateParams["articoliHome"][$i]["Nome_Articolo"] ?></div>
                                    </a>
                                </div>
                                <?php endfor; ?>
                            </div>
                        </div>


                        <div class=" col-12 col-sm-12">
                            <div class="row">
                                <?php for(; $i<4 ; $i++): ?>
                                <div class="img-hover-zoom row-image col-6 col-sm-6">
                                    <a href="article.php?id=<?php echo $templateParams["articoliHome"][$i]["Nome_Articolo"] ?>">
                                        <img src="<?php echo UPLOAD_DIR.$templateParams["articoliHome"][$i]["Codice_Immagine"]?>" class="img-fluid" alt="">
                                        <div class="text-hover" style="font-size: 15px"><?php echo $templateParams["articoliHome"][$i]["Nome_Articolo"] ?></div>
                                    </a>
                                </div>
                                <?php endfor; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="row-image col-12 col-sm-12 col-md-6">
                    <div class="row">
                    <div class="col-12 col-sm-12">
                            <div class="row">
                               <?php for($i=0; $i<2 ; $i++): ?>
                                <div class="img-hover-zoom col-6 col-sm-6">
                                    <a href="article.php?id=<?php echo $templateParams["articoliCollane"][$i]["Nome_Articolo"]?>">
                                        <img src="<?php echo UPLOAD_DIR.$templateParams["articoliCollane"][$i]["Codice_Immagine"]?>" class="img-fluid" alt="">
                                        <div class="text-hover" style="font-size: 15px"><?php echo $templateParams["articoliCollane"][$i]["Nome_Articolo"] ?> </div>
                                    </a>
                                </div>
                                <?php endfor; ?>
                            </div>
                        </div>


                        <div class=" col-12 col-sm-12">
                            <div class="row">
                                <?php for(; $i<4 ; $i++): ?>
                                <div class="img-hover-zoom row-image col-6 col-sm-6">
                                    <a href="article.php?id=<?php echo $templateParams["articoliCollane"][$i]["Nome_Articolo"]?>">
                                        <img src="<?php echo UPLOAD_DIR.$templateParams["articoliCollane"][$i]["Codice_Immagine"]?>" class="img-fluid" alt="">
                                        <div class="text-hover" style="font-size: 15px"><?php echo $templateParams["articoliCollane"][$i]["Nome_Articolo"] ?></div>
                                    </a>
                                </div>
                                <?php endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="img-hover-zoom row-image col-12 col-sm-12 col-md-6">
                <a href="category.php?id=Collane">
                    <img src="<?php echo UPLOAD_DIR.$templateParams["categorieCollane"][0]["Codice_Immagine"]?>" class="img-fluid" alt="">
                    <div class="text-hover" style="font-size: 50px;"><?php echo $templateParams["categorieCollane"][0]["Nome_Categoria"]?></div>
                </a>
                </div>
            </div>

    </div>