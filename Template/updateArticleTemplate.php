<section class="container my-5">
        <div class="text-center">
            <h3>Aggiorna l'articolo</h3>
        </div>
        <?php if(isset($templateParams["msg"])):?>
            <p><?php echo $templateParams["msg"]?></p>
        <?php endif;?>
        <form class="mt-5" method="POST" enctype="multipart/form-data">
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupPrezzo">Prezzo:</label>
                    <input name="prezzo" type="text" value="<?php echo $templateParams["infobase"]["Prezzo"]?>" class="form-control" id="formGroupPrezzo">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupPrezzo">Immagine:</label>
                    <input name="imgarticolo" type="file"  class="form-control" id="imgarticolo">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupPeso">Peso:</label>
                    <input name="peso" type="text" value="<?php echo $templateParams["infobase"]["Peso"]?>" class="form-control" id="formGroupPeso">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupScorta">Scorta:</label>
                    <input name="scorta" type="text" value="<?php echo $templateParams["infobase"]["Scorta"]?>" class="form-control" id="formGroupScorta">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupSconto">Sconto:</label>
                    <input name="sconto" type="text" class="form-control" value="<?php echo $templateParams["infobase"]["Sconto"]?>" id="formGroupSconto">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupDescrizione">Descrizione:</label>
                    <input name="descrizione" type="text" class="form-control" value="<?php echo $templateParams["infobase"]["Descrizione"]?>" id="formGroupDescrizione">
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <label for="formGroupMateriale">Materiale:</label>
                    <input name="materiale" type="text" value="<?php echo $templateParams["infobase"]["Materiale"]?>" class="form-control" id="formGroupMateriale">
                </div>
            </div>
            <div class="row justify-content-center mt-4">
                <button type="submit" class="btn btn-primary">Aggiorna articolo</button>
            </div>
        </form>
    </section>